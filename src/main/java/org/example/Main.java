package org.example;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        ModeType modeType = ModeType.HYBRID;
        List<IModeType> modeTypeValidModeTypeList = List.of(new ManualModeType(), new AutomatModeType());

        new ModeTypeProcessor(modeTypeValidModeTypeList).findModyType(modeType).process();
    }
}