package org.example;

import java.util.logging.Logger;

public class ManualModeType implements IModeType {
    Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public boolean isValidModeType(ModeType modeType) {
        return modeType == ModeType.MANUAL;
    }

    @Override
    public void process() {
        log.info("process manual mode");
    }
}
