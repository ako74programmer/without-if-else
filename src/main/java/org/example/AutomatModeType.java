package org.example;

import java.util.logging.Logger;

public class AutomatModeType implements IModeType  {
    Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public boolean isValidModeType(ModeType modeType) {
        return modeType == ModeType.AUTOMAT;
    }

    @Override
    public void process() {
       log.info("process automat mode");
    }
}
