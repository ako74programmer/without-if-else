package org.example;

import java.util.List;
import java.util.NoSuchElementException;

public class ModeTypeProcessor {

    private final List<IModeType> modeTypeValidModeTypeList;

    public ModeTypeProcessor(List<IModeType> modeTypeValidModeTypeList) {
        this.modeTypeValidModeTypeList = modeTypeValidModeTypeList;
    }

    IModeType findModyType(ModeType modeType) {
        return modeTypeValidModeTypeList.stream().filter(mt -> mt.isValidModeType(modeType)).findFirst()
                .orElseThrow(() -> new NoSuchElementException(modeType.name()));

    }
}