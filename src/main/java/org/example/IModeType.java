package org.example;

public interface IModeType {
    boolean isValidModeType(ModeType modeType);

    void process();
}
